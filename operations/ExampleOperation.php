<?php

use DbMig\Operation;

class ExampleOperation extends Operation {
    public $trigger = Operation::TRIGGER_BEFORE_MIGRATIONS;

    /**
     * Called when the operation is ran
     *
     * @param array $args
     */
    public function run(array $args)
    {
        $this->cli
            ->bold()
            ->red("Test operation called");
    }
}
