<?php



use DbMig\Migration;

class InboundEmailMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "inbound_email";

}
