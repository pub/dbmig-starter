<?php

use DbMig\RelationshipMigration;

class ContactsCasesMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "contact";

    public $rhSingular = "case";

    public $sourceTable = "contacts_cases";
}
