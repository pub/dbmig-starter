<?php


use DbMig\Migration;

class ProductMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "products";


}
