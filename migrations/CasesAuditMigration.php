<?php

use DbMig\AuditMigration;

class CasesAuditMigration extends AuditMigration
{
    public $truncateDestinationTable = true;

    public $parentTable = "cases";

}
