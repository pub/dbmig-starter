<?php

class ProjectTasksMigration extends \DbMig\Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "project_task";

}
