<?php


use DbMig\Migration;

class ProductTypesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "product_types";

}
