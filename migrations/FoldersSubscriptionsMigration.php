<?php


use DbMig\Migration;

class FoldersSubscriptionsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "folders_subscriptions";

}
