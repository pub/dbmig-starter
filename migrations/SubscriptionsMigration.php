<?php

class SubscriptionsMigration extends \DbMig\Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable= 'subscriptions';
}
