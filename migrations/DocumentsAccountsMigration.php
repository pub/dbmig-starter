<?php



use DbMig\RelationshipMigration;

class DocumentsAccountsMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "document";
    
    public $rhSingular = "account";
}