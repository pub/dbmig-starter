<?php



use DbMig\Migration;

class EmailsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "emails";
}