<?php



use DbMig\Migration;

class DocumentRevisionsMigration extends Migration
{
    public $truncateDestinationTable = true;

    var $sourceTable = "document_revisions";
}