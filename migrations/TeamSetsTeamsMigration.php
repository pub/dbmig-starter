<?php

use DbMig\Migration;

class TeamSetsTeamsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = 'team_sets_teams';
}