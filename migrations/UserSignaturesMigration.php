<?php

use DbMig\Migration;

class UserSignaturesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "users_signatures";
}