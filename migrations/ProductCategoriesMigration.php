<?php


use DbMig\Migration;

class ProductCategoriesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "product_categories";

}
