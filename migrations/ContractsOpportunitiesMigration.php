<?php

use DbMig\RelationshipMigration;

class ContractsOpportunitiesMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "contract";
    
    public $rhSingular = "opportunity";
    
    public $rhPlural = "opportunities";
}