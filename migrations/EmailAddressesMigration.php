<?php



use DbMig\Migration;

class EmailAddressesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "email_addresses";
}