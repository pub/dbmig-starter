<?php

use DbMig\Migration;

class AccountsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "accounts";
}