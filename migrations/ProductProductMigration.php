<?php


use DbMig\Migration;

class ProductProductMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "product_product";

}
