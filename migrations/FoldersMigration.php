<?php



use DbMig\Migration;

class FoldersMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "folders";

}
