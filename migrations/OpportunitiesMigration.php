<?php



use DbMig\Migration;

class OpportunitiesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "opportunities";
}