<?php

use DbMig\Migration;

class WorkflowActionShellsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "workflow_actionshells";

}
