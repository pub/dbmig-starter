<?php

class ContactsCustomMigration extends DbMig\CustomMigration
{
    public $truncateDestinationTable = true;

    public $parentTable = 'contacts';

}
