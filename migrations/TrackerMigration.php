<?php


use DbMig\Migration;

class TrackerMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "tracker";
}
