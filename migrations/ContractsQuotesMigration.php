<?php

use DbMig\RelationshipMigration;

class ContractsQuotesMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "contract";
    
    public $rhSingular = "quote";
}