<?php

use DbMig\RelationshipMigration;

class ContactsUsersMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "contact";
    
    public $rhSingular = "user";
}