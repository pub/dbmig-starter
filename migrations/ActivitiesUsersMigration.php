<?php


class ActivitiesUsersMigration extends \DbMig\Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "activities_users";
}
