<?php

use DbMig\Migration;

class TeamSetsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "team_sets";
}