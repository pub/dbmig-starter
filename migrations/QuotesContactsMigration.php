<?php



use DbMig\RelationshipMigration;

class QuotesContactsMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "quote";

    public $rhSingular = "contact";
    
    public $additionalSourceColumns = [
        "contact_role",
    ];
}