<?php

use DbMig\AuditMigration;

class ContractsAuditMigration extends AuditMigration
{
    public $truncateDestinationTable = true;

    public $parentTable = "contracts";
}