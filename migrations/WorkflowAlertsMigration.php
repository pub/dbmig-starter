<?php

use DbMig\Migration;

class WorkflowAlertsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "workflow_alerts";

}
