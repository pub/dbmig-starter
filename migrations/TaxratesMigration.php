<?php



use DbMig\Migration;

class TaxratesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "taxrates";
}