<?php



use DbMig\Migration;

class ForecastsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "forecasts";

}
