<?php

use DbMig\Migration;

require_once 'ContactsMigration.php';


class ManufacturersMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "manufacturers";

}
