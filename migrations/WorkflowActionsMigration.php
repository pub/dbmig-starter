<?php

use DbMig\Migration;

class WorkflowActionsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "workflow_actions";

}
