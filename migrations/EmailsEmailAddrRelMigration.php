<?php



use DbMig\Migration;
use Latitude\QueryBuilder\Conditions;
use Latitude\QueryBuilder\SelectQuery;

class EmailsEmailAddrRelMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "emails_email_addr_rel";

    protected function extendSelect(SelectQuery $q, Conditions $where)
    {
        parent::extendSelect($q, $where);

        $where->andWith("`{$this->sourceTable}`.`email_id` IN (SELECT `id` FROM `emails` WHERE `emails`.`deleted` = 0 AND `emails`.`id` = `{$this->sourceTable}`.`email_id`)");
        $where->andWith("`{$this->sourceTable}`.`email_address_id` IN (SELECT `id` FROM `email_addresses` WHERE `email_addresses`.`deleted` = 0 AND `email_addresses`.`id` = `{$this->sourceTable}`.`email_address_id`)");
    }
}