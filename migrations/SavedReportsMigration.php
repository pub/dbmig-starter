<?php

class SavedReportsMigration extends \DbMig\Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable= 'saved_reports';
}
