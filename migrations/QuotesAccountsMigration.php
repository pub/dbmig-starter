<?php



use DbMig\RelationshipMigration;

class QuotesAccountsMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "quote";

    public $rhSingular = "account";
    
    public $additionalSourceColumns = [
        "account_role",
    ];
}