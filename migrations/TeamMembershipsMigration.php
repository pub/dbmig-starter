<?php

use DbMig\Migration;

class TeamMembershipsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "team_memberships";
}