<?php



use DbMig\RelationshipMigration;

class OpportunitiesContactsMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "opportunity";
    
    public $lhPlural = "opportunities";
    
    public $rhSingular = "contact";

    public $additionalSourceColumns = [
        "contact_role",
    ];
}