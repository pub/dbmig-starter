<?php



use DbMig\Migration;

class ProspectListsProspectsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "prospect_lists_prospects";
}