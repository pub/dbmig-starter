<?php



use DbMig\Migration;

class KbcontentsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "kbcontents";

}
