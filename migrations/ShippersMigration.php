<?php



use DbMig\Migration;

class ShippersMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "shippers";
}