<?php



use DbMig\Migration;

class DocumentsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "documents";
}