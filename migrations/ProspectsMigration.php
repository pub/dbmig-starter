<?php



use DbMig\Migration;

class ProspectsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "prospects";
}