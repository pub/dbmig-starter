<?php



use DbMig\RelationshipMigration;

class QuotesOpportunitiesMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "quote";

    public $rhSingular = "opportunity";

    public $rhPlural = "opportunities";
}