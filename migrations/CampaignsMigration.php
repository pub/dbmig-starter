<?php

use DbMig\Migration;

class CampaignsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "campaigns";
}