<?php



use DbMig\Migration;

class AclRoleSetsAclRolesMigration extends Migration
{
    public $truncateDestinationTable = false;

    public $sourceTable = "acl_role_sets_acl_roles";
}
