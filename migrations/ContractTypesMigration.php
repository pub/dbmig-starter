<?php



use DbMig\Migration;

class ContractTypesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "contract_types";
}