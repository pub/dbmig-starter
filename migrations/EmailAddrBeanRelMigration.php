<?php



use DbMig\Migration;

class EmailAddrBeanRelMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "email_addr_bean_rel";
}