<?php

use DbMig\RelationshipMigration;

class CallsContactsMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "call";

    public $rhSingular = "contact";

    public $sourceTable = "calls_contacts";

    public $additionalSourceColumns = [
        "required",
        "accept_status",
    ];
}