<?php

class OpportunitiesCustomMigration extends DbMig\CustomMigration
{
    public $truncateDestinationTable = true;

    public $parentTable = 'opportunities';

}
