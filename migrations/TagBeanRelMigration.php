<?php



class TagBeanRelMigration extends \DbMig\Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "tag_bean_rel";
}
