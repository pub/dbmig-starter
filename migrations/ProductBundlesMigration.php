<?php


use DbMig\Migration;

class ProductBundlesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "product_bundles";


}
