<?php

use DbMig\Migration;

class UsersMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "users";
}