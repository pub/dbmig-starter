<?php

use DbMig\Migration;

class TeamsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "teams";
}