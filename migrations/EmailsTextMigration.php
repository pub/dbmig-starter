<?php

use DbMig\Migration;
use Latitude\QueryBuilder\Conditions;
use Latitude\QueryBuilder\SelectQuery;

class EmailsTextMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "emails_text";

    protected function extendSelect(SelectQuery $q, Conditions $where)
    {
        parent::extendSelect($q, $where);

        $where->andWith("`{$this->sourceTable}`.`email_id` IN (SELECT `id` FROM `emails` WHERE `emails`.`deleted` = 0 AND `emails`.`id` = `{$this->sourceTable}`.`email_id`)");
    }
}