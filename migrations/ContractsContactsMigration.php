<?php

use DbMig\RelationshipMigration;

class ContractsContactsMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "contract";
    
    public $rhSingular = "contact";
}