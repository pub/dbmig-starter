<?php



use DbMig\Migration;

class NotesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "notes";
}