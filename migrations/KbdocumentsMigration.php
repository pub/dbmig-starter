<?php



use DbMig\Migration;

class KbdocumentsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "kbdocuments";

}
