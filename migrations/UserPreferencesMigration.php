<?php

use DbMig\Migration;

class UserPreferencesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "user_preferences";
}