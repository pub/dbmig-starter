<?php



use DbMig\RelationshipMigration;

class DocumentsOpportunitiesMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "document";
    
    public $rhSingular = "opportunity";
    
    public $rhPlural = "opportunities";
}