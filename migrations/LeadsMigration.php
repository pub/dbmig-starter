<?php



use DbMig\Migration;

class LeadsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "leads";
}