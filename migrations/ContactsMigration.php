<?php

use DbMig\Migration;

class ContactsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "contacts";

}