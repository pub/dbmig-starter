<?php



use DbMig\Migration;

class FiltersMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "filters";

}
