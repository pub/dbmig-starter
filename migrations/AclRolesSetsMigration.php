<?php



use DbMig\Migration;

class AclRolesSetsMigration extends Migration
{
    public $truncateDestinationTable = false;

    public $sourceTable = "acl_role_sets";
}
