<?php



use DbMig\Migration;

class ExpressionsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "expressions";

}
