<?php



use DbMig\Migration;

class DashboardMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "dashboards";

}
