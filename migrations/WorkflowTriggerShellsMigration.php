<?php

use DbMig\Migration;

class WorkflowTriggerShellsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "workflow_triggershells";

}
