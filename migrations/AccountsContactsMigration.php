<?php

use DbMig\RelationshipMigration;

class AccountsContactsMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "contact";

    public $rhSingular = "account";

    public $sourceTable = "accounts_contacts";

    public $destinationTable = "accounts_contacts";

    public function processRow(array $row)
    {
        $row = parent::processRow($row);

        $row["primary_account"] = 1;

        return $row;
    }
}