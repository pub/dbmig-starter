<?php

use DbMig\Migration;

class ContractsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "contracts";
}