<?php



use DbMig\Migration;

class KbarticlesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "kbarticles";

}
