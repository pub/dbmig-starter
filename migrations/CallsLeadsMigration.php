<?php

use DbMig\RelationshipMigration;

class CallsLeadsMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "call";

    public $rhSingular = "lead";

    public $sourceTable = "calls_leads";

    public $additionalSourceColumns = [
        "required",
        "accept_status",
    ];
}