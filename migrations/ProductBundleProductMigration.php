<?php


use DbMig\Migration;

class ProductBundleProductMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "product_bundle_product";


}
