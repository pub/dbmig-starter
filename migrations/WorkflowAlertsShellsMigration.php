<?php

use DbMig\Migration;

class WorkflowAlertsShellsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "workflow_alertshells";

}
