<?php

class ReportSchedulersMigration extends \DbMig\Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable= 'report_schedules';
}
