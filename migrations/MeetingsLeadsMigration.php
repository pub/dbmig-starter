<?php



use DbMig\RelationshipMigration;

class MeetingsLeadsMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "meeting";

    public $rhSingular = "lead";

    public $additionalSourceColumns = [
        "required",
        "accept_status",
    ];
}