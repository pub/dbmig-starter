<?php



use DbMig\RelationshipMigration;

class MeetingsUsersMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "meeting";
    
    public $rhSingular = "user";

    public $additionalSourceColumns = [
        "required",
        "accept_status",
    ];
}