<?php



use DbMig\RelationshipMigration;

class DocumentsQuotesMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "document";
    
    public $rhSingular = "quote";
}