<?php



use DbMig\Migration;

class EmailMarketingMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "email_marketing";
    
    public function processRow(array $row)
    {
        $dateStart = new \DateTime($row["date_start"], new \DateTimeZone("UTC"));
        if ($dateStart->getTimestamp() < 0) {
            // Invalid date, set to null
            $row["date_start"] = null;
        }

        return $row;
    }
}