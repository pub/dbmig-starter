<?php

use DbMig\AuditMigration;

class BugsAuditMigration extends AuditMigration
{
    public $truncateDestinationTable = true;

    public $parentTable = "bugs";
    public $sourceTable = "bugs_audit";

}
