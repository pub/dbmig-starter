<?php


class SavedSearchMigration extends \DbMig\Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable= 'saved_search';
}
