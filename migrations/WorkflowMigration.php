<?php

use DbMig\Migration;

class WorkflowMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "workflow";

}
