<?php

class EmailTemplatesCustomMigration extends DbMig\CustomMigration
{
    public $truncateDestinationTable = true;

    public $parentTable = 'email_templates';

}

