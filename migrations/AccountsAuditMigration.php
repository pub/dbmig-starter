<?php

use DbMig\AuditMigration;

class AccountsAuditMigration extends AuditMigration
{
    public $truncateDestinationTable = true;

    public $parentTable = "accounts";
}