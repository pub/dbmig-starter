<?php



use DbMig\Migration;

class ForecastManagerWorksheetsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "forecast_manager_worksheets";

}
