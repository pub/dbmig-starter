<?php



use DbMig\AuditMigration;

class QuotesAuditMigration extends AuditMigration
{
    public $truncateDestinationTable = true;

    public $parentTable = "quotes";
}