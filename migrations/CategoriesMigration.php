<?php

use DbMig\Migration;

class CategoriesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "categories";

}
