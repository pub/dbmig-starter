<?php

use DbMig\RelationshipMigration;

class CallsUsersMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "call";
    
    public $rhSingular = "user";
    
    public $sourceTable = "calls_users";

    public $additionalSourceColumns = [
        "required",
        "accept_status",
        "date_modified",
    ];
}