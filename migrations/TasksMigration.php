<?php



use DbMig\Migration;

class TasksMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "tasks";
}