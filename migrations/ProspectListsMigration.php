<?php



use DbMig\Migration;

class ProspectListsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "prospect_lists";
}