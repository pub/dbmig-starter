<?php

use DbMig\Migration;

class ProjectMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "project";

    public $ignoreSourceColumns = [
    ];

}
