<?php

use DbMig\AuditMigration;

class ContactsAuditMigration extends AuditMigration
{
    public $truncateDestinationTable = true;

    public $parentTable = "contacts";
}