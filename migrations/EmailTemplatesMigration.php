<?php



use DbMig\Migration;

class EmailTemplatesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "email_templates";
}