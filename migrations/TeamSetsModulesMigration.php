<?php

use DbMig\Migration;

class TeamSetsModulesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = 'team_sets_modules';
}