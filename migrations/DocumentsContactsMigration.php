<?php



use DbMig\RelationshipMigration;

class DocumentsContactsMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "document";
    
    public $rhSingular = "contact";
}