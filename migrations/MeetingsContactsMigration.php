<?php



use DbMig\RelationshipMigration;

class MeetingsContactsMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "meeting";
    
    public $rhSingular = "contact";

    public $additionalSourceColumns = [
        "required",
        "accept_status",
    ];
}