<?php



use DbMig\Migration;
use Latitude\QueryBuilder\Conditions;
use Latitude\QueryBuilder\SelectQuery;

class EmailsBeansMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "emails_beans";

    protected function extendSelect(SelectQuery $q, Conditions $where)
    {
        parent::extendSelect($q, $where);

        $where->andWith("`{$this->sourceTable}`.`email_id` IN (SELECT id FROM `emails` WHERE `emails`.`deleted` = 0 AND `emails`.`id` = `{$this->sourceTable}`.`email_id`)");
    }
}