<?php



use DbMig\RelationshipMigration;

class RolesUsersMigration extends RelationshipMigration
{
    public $lhSingular = "role";
    
    public $rhSingular = "user";
}