<?php



use DbMig\Migration;

class MeetingsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "meetings";

    public function processRow(array $row)
    {
        $dateStart = new \DateTime($row["date_start"], new \DateTimeZone("UTC"));
        if ($dateStart->getTimestamp() < 0) {
            // Invalid date, just set to null
            $row["date_start"] = null;
        }

        return $row;
    }
}