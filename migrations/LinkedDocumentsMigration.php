<?php



use DbMig\Migration;

class LinkedDocumentsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = 'linked_documents';
}