<?php


use DbMig\Migration;

class ProductTemplatesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "product_templates";

}
