<?php



use DbMig\Migration;

class SystemsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "systems";
}