<?php

use DbMig\AuditMigration;

class CampaignsAuditMigration extends AuditMigration
{
    public $truncateDestinationTable = true;

    public $parentTable = "campaigns";
}