<?php

use DbMig\RelationshipMigration;

class AccountsOpportunitiesMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    var $lhSingular = "account";

    var $rhSingular = "opportunity";

    var $rhPlural = "opportunities";

    public $sourceTable = "accounts_opportunities";
}