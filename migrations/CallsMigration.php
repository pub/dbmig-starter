<?php

use DbMig\Migration;

class CallsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "calls";
}