<?php



use DbMig\AuditMigration;

class LeadsAuditMigration extends AuditMigration
{
    public $truncateDestinationTable = true;

    public $parentTable = "leads";
}