<?php

use DbMig\Migration;
use Latitude\QueryBuilder\Conditions;
use Latitude\QueryBuilder\SelectQuery;

class CampaignTrkrsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "campaign_trkrs";

    protected function extendSelect(SelectQuery $q, Conditions $where)
    {
        parent::extendSelect($q, $where);

        $where->andWith("`{$this->sourceTable}`.`campaign_id` IN (SELECT id FROM campaigns WHERE deleted = 0 AND id = `{$this->sourceTable}`.`campaign_id`)");
    }
}