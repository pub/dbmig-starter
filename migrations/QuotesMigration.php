<?php



use DbMig\Migration;

class QuotesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "quotes";
}