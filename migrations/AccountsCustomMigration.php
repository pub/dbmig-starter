<?php

class AccountsCustomMigration extends DbMig\CustomMigration
{
    public $truncateDestinationTable = true;

    public $parentTable = 'accounts';

}
