<?php



use DbMig\Migration;

class CurrenciesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "currencies";
}