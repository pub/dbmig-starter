<?php

use DbMig\Migration;

class ActivitiesMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "activities";

}
