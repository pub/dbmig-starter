<?php



use DbMig\AuditMigration;

class OpportunitiesAuditMigration extends AuditMigration
{
    public $truncateDestinationTable = true;

    public $parentTable = "opportunities";
}