<?php

class LeadsCustomMigration extends DbMig\CustomMigration
{
    public $truncateDestinationTable = true;

    public $parentTable = 'leads';

}
