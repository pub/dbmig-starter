<?php



use DbMig\RelationshipMigration;

class ProspectListCampaignsMigration extends RelationshipMigration
{
    public $truncateDestinationTable = true;

    public $lhSingular = "prospect_list";
    
    public $rhSingular = "campaign";
    
    public $sourceTable = "prospect_list_campaigns";
}