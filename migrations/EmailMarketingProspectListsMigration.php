<?php



use DbMig\Migration;

class EmailMarketingProspectListsMigration extends Migration
{
    public $truncateDestinationTable = true;

    public $sourceTable = "email_marketing_prospect_lists";
}