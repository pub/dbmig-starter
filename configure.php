#! env php

<?php

require_once "vendor/autoload.php";

use League\CLImate\CLImate;

function rrmdir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir . "/" . $object))
                    rrmdir($dir . "/" . $object);
                else
                    unlink($dir . "/" . $object);
            }
        }
        rmdir($dir);
    }
}

function readDbConfig(string $prefix, CLImate $cli)
{
    $host = $cli->input("Host [localhost]:")
        ->prompt();

    if (!$host) {
        $host = "localhost";
    }

    $port = $cli->input("Port [3306]:")
        ->prompt();

    if (!$port || $port <= 0) {
        $port = 3306;
    }

    do {
        $name = $cli->input("Name:")
            ->prompt();
    } while (!$name);

    do {
        $user = $cli->input("User:")
            ->prompt();
    } while (!$user);

    $pass = $cli->password("Password:")
        ->prompt();

    return [
        "\$${prefix}Dsn" => "mysql:host=$host;port=$port;dbname=$name",
        "\$${prefix}User" => $user,
        "\$${prefix}Pass" => $pass,
    ];
}

$cli = new CLImate();
$cli->forceAnsiOn();

$cli->bold()
    ->blue()
    ->border("=")
    ->bold()
    ->blue("DbMig starter configuration")
    ->bold()
    ->blue()
    ->border("=")
    ->br();

do {
    $projectName = $cli->input("Project name:")
        ->prompt();
} while (!$projectName);


// Read source database settings
$cli->br()
    ->br()
    ->blue()
    ->border("-")
    ->bold()
    ->blue("Source database")
    ->blue()
    ->border("-")
    ->br();

$src = readDbConfig("src", $cli);


// Read destination database settings
$cli->br()
    ->br()
    ->blue()
    ->border("-")
    ->bold()
    ->blue("Destination database")
    ->blue()
    ->border("-")
    ->br();

$dst = readDbConfig("dst", $cli);


// Create .env file
$dotEnv = file_get_contents(__DIR__ . "/.env.template");
$dotEnv = strtr($dotEnv, array_merge($src, $dst));
file_put_contents(__DIR__ . "/.env", $dotEnv);

// Create new composer.json
$composerJson = file_get_contents(__DIR__ . "/composer.tpl.json");
$composerJson = strtr($composerJson, ['$projectName' => $projectName]);
file_put_contents(__DIR__ . "/composer.json", $composerJson);

// Remove old files
unlink(__DIR__ . "/composer.lock");
unlink(__DIR__ . "/configure.php");
unlink(__DIR__ . "/composer.tpl.json");
unlink(__DIR__ . "/.env.template");

rrmdir(__DIR__ . "/.git");

exec("composer install");
