#! /usr/bin/env php

<?php

require_once "vendor/autoload.php";

// Read migrations to run from argv
// If none given all are run
$migrations = array_splice($argv, 1);

$migrator = new \DbMig\Migrator(__DIR__);

// Enable truncation if required
// $migrator->enableTruncation();

$success = $migrator->run((count($migrations) > 0) ? $migrations : null);

// Or to run only certain migrations
// $success = $migrator->run(["AccountMigration", "LeadsMigration"]);

exit(($success) ? 0 : -1);
